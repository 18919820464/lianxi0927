package com.lq0927.lq0927.controller;

import com.lq0927.lq0927.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: OrderController
 * @author: xutao
 * @createDate: 2023-04-14
 * @version: 1.0
 */
@Controller
public class OrderController {
    @Autowired
    private OrderService orderService;
    @RequestMapping(value = "/order/test")
    public String getOrder(){
        int i = 10 ;
        return "success";
    }
}
